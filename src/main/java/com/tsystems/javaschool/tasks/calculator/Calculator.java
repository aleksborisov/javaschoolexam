package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.EmptyStackException;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (checkStatement(statement)) {
            return rpnToResult(statementToRpn(statement));
        } else {
            return null;
        }
    }

    private boolean checkStatement(String statement) {

        if (statement == null || statement.isEmpty()) {
            return false;
        }

        int openingParenthesisCount = 0;
        int closingParenthesisCount = 0;

        for (int i = 0; i < statement.length(); i++) {
            if (closingParenthesisCount > openingParenthesisCount) {
                return false;
            }

            if (statement.charAt(i) == '(') {
                openingParenthesisCount++;
            } else if (statement.charAt(i) == ')') {
                closingParenthesisCount++;
            }
        }

        return openingParenthesisCount == closingParenthesisCount;
    }

    private String statementToRpn(String statement) {

        StringBuilder output = new StringBuilder();
        Stack<Character> stack = new Stack<>();
        int priority;

        for (int i = 0; i < statement.length(); i++) {
            char currentChar = statement.charAt(i);
            priority = getPriority(currentChar);

            if (priority == 0) {
                output.append(currentChar);
            }

            if (priority == 1) {
                stack.push(currentChar);
            }

            if (priority > 1) {
                output.append(" ");

                while (!stack.empty()) {
                    if (getPriority(stack.peek()) >= priority) {
                        output.append(stack.pop());
                    } else {
                        break;
                    }
                }

                stack.push(currentChar);
            }

            if (priority == -1) {
                output.append(" ");

                while (getPriority(stack.peek()) != 1) {
                    output.append(stack.pop());
                }

                stack.pop();
            }
        }

        while (!stack.empty()) {
            output.append(' ');
            output.append(stack.pop());
        }

        return output.toString().trim();
    }

    private String rpnToResult(String rpn) {

        StringBuilder operand = new StringBuilder();
        Stack<Double> stack = new Stack<>();

        for (int i = 0; i < rpn.length(); i++) {
            if (rpn.charAt(i) == ' ') {
                continue;
            }

            if (getPriority(rpn.charAt(i)) == 0) {
                while (rpn.charAt(i) != ' ' && getPriority(rpn.charAt(i)) == 0) {
                    operand.append(rpn.charAt(i++));

                    if (i == rpn.length()) {
                        break;
                    }
                }

                try {
                    stack.push(Double.parseDouble(operand.toString()));
                } catch (NumberFormatException e) {
                    return null;
                }

                operand = new StringBuilder();
            }

            if (getPriority(rpn.charAt(i)) > 1) {
                double right;
                double left;

                try {
                    right = stack.pop();
                    left = stack.pop();
                } catch (EmptyStackException e) {
                    return null;
                }

                if (rpn.charAt(i) == '+') {
                    stack.push(left + right);
                }

                if (rpn.charAt(i) == '-') {
                    stack.push(left - right);
                }

                if (rpn.charAt(i) == '*') {
                    stack.push(left * right);
                }

                if (rpn.charAt(i) == '/') {
                    if (right == 0.0) {
                        return null;
                    }

                    stack.push(left / right);
                }
            }
        }

        return new DecimalFormat("#.####", DecimalFormatSymbols.getInstance(Locale.ENGLISH)).format(stack.pop());
    }

    private int getPriority(char token) {

        switch (token) {
            case '*':
            case '/':
                return 3;
            case '+':
            case '-':
                return 2;
            case '(':
                return 1;
            case ')':
                return -1;
            default:
                return 0;
        }
    }
}
